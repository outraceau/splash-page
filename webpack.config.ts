import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import { join as pathJoin, normalize as pathNormalize } from "path";
import { Configuration, Loader } from "webpack";

const isProduction = process.env.NODE_ENV === "production";

const LoaderResponsive: Loader = {
    loader: "responsive-loader",
    options: {
        adapter: require("responsive-loader/sharp"),
        sourceMap: !isProduction,
    },
};

const LoaderFile: Loader = {
    loader: "file-loader",
};

const LoaderUrl: Loader = {
    loader: "url-loader",
    options: {
        fallback: LoaderFile,
        limit: 8192,
    },
};

const LoaderPostCSS: Loader = {
    loader: "postcss-loader",
    options: {
        ident: "postcss",
        plugins: (): any[] => [
            require("postcss-preset-env")(),
            ...(
                isProduction ? [
                    require("cssnano")(),
                ] : []
            ),
        ],
        sourceMap: !isProduction,
    },
};

const LoaderCSS: Loader = {
    loader: "css-loader",
    options: {
        importLoaders: 1,
        sourceMap: !isProduction,
    },
};

const LoaderTypeScript: Loader = {
    loader: "ts-loader",
    options: {
        compilerOptions: {
            module: "esnext",
            sourceMap: !isProduction,
            target: "es5",
        },
        transpileOnly: true,
    },
};

const LoaderMiniCSS: Loader = MiniCssExtractPlugin.loader;

const LoaderHTML: Loader = {
    loader: "html-loader",
    options: {
        minimize: isProduction,
    },
};

module.exports = {
    devtool: isProduction ? false : "source-map",
    entry: [
        pathNormalize(pathJoin(__dirname, "src", "index.css")),
        pathNormalize(pathJoin(__dirname, "src", "index.ts")),
    ],
    mode: isProduction ? "production" : "development",
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.[jt]sx?$/i,
                use: [
                    LoaderTypeScript,
                ],
            },
            {
                test: /\.css$/i,
                use: [
                    LoaderMiniCSS,
                    LoaderCSS,
                    LoaderPostCSS,
                ],
            },
            {
                test: /\.(gif|ttf|otf|eot|svg|woff2?)$/i,
                use: [
                    LoaderUrl,
                ],
            },
            {
                test: /\.(png|jpe?g)$/i,
                use: [
                    LoaderResponsive,
                    LoaderUrl,
                ],
            },
            {
                test: /\.html$/i,
                use: [
                    LoaderHTML,
                ],
            },
        ],
    },
    optimization: {
        mergeDuplicateChunks: true,
        minimize: isProduction,
        removeAvailableModules: true,
        removeEmptyChunks: true,
        splitChunks: {
            chunks: "async",
        },
    },
    output: {
        path: pathNormalize(pathJoin(__dirname, "dist")),
        publicPath: "/",
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: pathNormalize(pathJoin(__dirname, "src", "index.html")),
        }),
        new MiniCssExtractPlugin(),
    ],
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        mainFields: ["browser", "main", "module"],
    },
    target: "web",
} as Configuration;
