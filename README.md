# Paradigm

to build the app execute following commands:

``` sh
npm install # to install the dependencies
npm run build # actual build step
```

you can also use `yarn` instead of `npm`

## FAQ

* what are the build requirements?

  you need to have installed `yarn`, `npm` or another `node` package manager capable of reading the content of the `package.json` file

* I've builded the app, what now?

  files ready to use in preduction environemt are placed in `dist/` directory, do what you want with them

* I want to make a change, do I need a developer for that?
  
  it depends how big the change is, but if you just want to correct some details in `html` file feel free to do it yourself, its relatively easy.

* how to make a change?
  
  you need to locate the subject of change in the `src/` directory and apply it to the source code; to preview the changes, first you need to install the dependencies (`npm install`) and run the development server (`npm start`), then you can preview your changes in the browser by visiting [localhost:8080](http://localhost:8080/), don't forget to commint and push your changes, ideally by creating a pull request
